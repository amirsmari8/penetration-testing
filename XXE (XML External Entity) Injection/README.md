# XML general 
XML stands for "Extensible Markup Language." It is a markup language that is designed to store and transport data in a structured format. XML data consists of a set of rules for encoding documents in a format that is both human-readable and machine-readable.
XML data is typically used to represent structured data in a variety of applications, such as web services, databases, and electronic data interchange (EDI) systems. XML data can be used to describe data structures, such as lists, trees, and graphs, as well as to encode documents, such as web pages, e-books, and scientific articles.
# XML vs JSON 
XML and JSON are both used for structuring and transmitting data, but they have some key differences. 
*  XML uses tags to define elements and attributes to describe them, while JSON uses braces to define objects and arrays to define collections of objects 
* JSON is typically smaller in size than XML, which can make it more efficient for transmitting data over networks 
* JSON parsing is generally faster and easier than XML parsing, which makes it a good choice for use in web applications and other systems that require fast data processing 
*  XML has a well-defined schema language (XSD) that allows for strict validation of document structure and data types. JSON does not have a built-in schema language 
* XML is more extensible than JSON, as it allows for custom elements and attributes to be defined. This makes XML a good choice for representing complex data structures and for use in enterprise-level systems 
# Example XML 
In this example, the XML data represents a bookstore that contains three books. Each book is represented by a <book> element, which contains nested <title>, <author>, and <price> elements. The category attribute is used to specify the genre of each book. 
# loacl entites vs External Entities 

Local entites: "amir" entity and it's value is the string example entity.
<?xml version="1.0" encoding="UTF-8"?> 
<!DOCTYPE test [ <!ENTITY amir "example entity"> ]>
<stockCheck>
  <productId>&amir;</productId>
  <storeId>1</storeId>
</stockCheck>

External entites: we can refrence to "file:///etc/passwd" or "http://169.254.169.254/", we add SYSYTEM

<?xml version="1.0" encoding="UTF-8"?> 
<!DOCTYPE test [ <!ENTITY amir SYSTEM "file:///etc/passwd"> ]>
<stockCheck>
  <productId>&amir;</productId>
  <storeId>1</storeId>
</stockCheck>

# XXE(XML External Entity) injection :
=> XXE injection can be used on web applications that parse XML input. (tof1) 
=> the attack occurs when the XML is processed by the server. 
# what cause XXE Injection 
=> XXE injections occur when improperly sanitized input is being proceessed by an XML parser with default or weakly configured settings 
# How to prevent XXE Injection 
* Apply application-wide filters or sanitization on all user-provided input, consider GET and POST parameters, Cookies and other HTTP headers. 
* Always apply whitelist input validation. 
* XML parsers should disable support for "external entities", or DTDs (Document Type Definition. A DTD defines the structure and the legal elements and attributes of an XML document) completely. Check framework specific setting to do this.
______________________________________________________________________________________________________________________
______________________________________________________________________________________________________________________
*** Lab: Exploiting XXE using external entities to retrieve files 

This lab has a "Check stock" feature that parses XML input and returns any unexpected values in the response.

To solve the lab, inject an XML external entity to retrieve the contents of the /etc/passwd file. 

*** Lab: Exploiting XXE to perform SSRF attacks 
note: to retrieve all instance metadata for amazon EC2 instance: 
http://169.254.169.254/latest/meta-data/