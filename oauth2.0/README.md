# General : 
* see the implementation of Oauth2 with nestjs 
OAuth 2.0 is an open standard for authorization. It allows users to share their private resources (e.g., photos, videos, contact lists) stored on one site with another site without having to hand out their credentials, typically a username and password. Instead, the user shares an access token that represents their authorization to access the desired resources.

# Oauth 4 roles:
The resource owner (e.g., the user)
The resource server (e.g., the API)  # your backend server
The client (e.g., the application requesting access to the user's resources) #your frontend
The authorization server (e.g., the service that issues access tokens)  #third-party application : like popular social media platform google, facebook ... 
The flow of an OAuth 2.0 authorization typically goes as follows:

# Oauth flow: 
The client requests authorization from the resource owner.
The resource owner grants or denies the request.
If the request is granted, the client receives an authorization grant (e.g., a code).
The client exchanges the authorization grant for an access token from the authorization server.
The client uses the access token to access the protected resources on the resource server. 

=== Terminology Used by oauth2: 
1- Authorization Code: is when we choose the specific "google account", and we grant access to some scopes provided (allow buttom in google).
The Authorization Code is exchanging for Access Token. 
2- Scope: the data to be accessed by app (for exmaple only email google and photo)
3- Redirect URL: is assign by  the server => the server redirect client to this URL as exmaple /auth
4- Access Token:
the server send it . 
using it we can request userdata as example photo de profil ... 


---- 
Oauth is under the Broken Access Control Vulnerabilities , the best way to discover it is to have 2 account and see if we can access information that belong to user1 from user2 . 

* Lab: Lab: Authentication bypass via OAuth implicit flow
This lab uses an OAuth service to allow users to log in with their social media account, it possible for an attacker to log in to other users' accounts without knowing their password. 
we have user wiener and we want to access the user carlos (carlos@carlos-montoya.net) 
there is a technique we use in burp suite is that some time we need to send the request with the browser not burp suite => right click > request in browser >  in original session. 
   

