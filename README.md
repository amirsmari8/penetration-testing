cp9J8^BB[8Dp&jWDc9.j%*3/yj72@Hd?
#### Definition: 
>>> vulnerability: vulnerability is a weakness that can be exploited by cybercriminals to gain unauthorized access to a computer system. 
>>> OWASP: Open Web Application Security Project (OWASP) is a nonprofit foundation dedicated to improving software security. 
>>> Threat: A cyber security threat refers to any possible malicious attack that seeks to unlawfully access data, disrupt digital operations or damage information. 
>>>> burpsuite intruder:  "Intruder" refers to a powerful tool used for performing automated attacks on web applications
### Practise Lab 
https://portswigger.net/web-security/all-labs
https://www.youtube.com/@RanaKhalil101/playlists

#### OWASP Top 10 vulnerability: 
1- Broken Acess Control 
2- Cryptographic Failures 
3- Injection 
4- Insecure Design 
5- Security Misconfiguration 
6- Vulnerable Outdated Components 
7- Identification and Authentication Failures 
8- Software and Data Integrity Failures 
9- Security Logging and Monitoring Failures 
10- Server Side Request Forgery(SSRF) 

#### Genaral: 
Encoding vs Encryption vs Hashing 
* Encoding: it's just data representation (no key require), like to encode secret in k8s. example: base64, URL, ASCII ...  
* Encryption: transformation of data into anothor format (require key: symetric 1 key for encryption or dycription, or asymetric) exapmle: AES,     DES ... 
* hashing: transforming data in one way (we can not revert the data ),but we will have the same output, no key require. example: MD5, SHA256 
* Safe http verbs: GET, HEAD, TRACE
--- 
# Query string:
 a query string is the entire part of the URL that starts with "?" and contains one or more query parameters,
--- 
# HTTP headers: 
* PATCH for making partial updates to a resource when you only want to modify specific fields or properties.
* PUT when you want to replace the entire resource or create a new one with the provided data.


HTTP Referer header: 
The Referer (also spelled "referrer") HTTP header field is used to indicate the address of the previous web page from which a link to the currently requested page was followed. The Referer field is sent by the browser with every request, and it can be used by a server to determine the origin of the request and to prevent unwanted cross-site requests.

For example, imagine a website "example.com" that has a link to "example2.com", when a user clicks on the link, the browser will make a request to "example2.com" with the Referer header set to "example.com", allowing the server of "example2.com" to know that the user came from "example.com".

In some cases, the browser may not send the Referer header, such as when the user types the URL directly into the address bar, or when the link is a "secure" link (i.e. HTTPS to HTTPS), or when the link is from a different origin (i.e. HTTP to HTTPS)

The Referer header can be used for security purpose, to check the origin of the request and prevent cross-site request forgery (CSRF) attacks. It can also be used for analytics and statistics, to track the referral source of the traffic.

---
Token vs Session vs OAuth:
--
Token: 
* Limited life 
* grant access to only a subset of the data 
* Authorization HTTP header.  
JWT(JSON WEB TOKEN ): contain 3 part: Header, payload, signature (headers and payload are encoded base|64)
the server will generate a jwt and store in "local storage or cookie " in the browsers . 
=> to get the signature we "hash" the headers + payload then encrypt the result of hash using "secret key". when a request come to server it will hash the headers and pyload and encrypt the hash using "the Key" and compare the signature with signature in the request itself. 
=>The server that issues the token typically generates the signature by hashing the header and payload. In some cases, they also encrypt the resulting hash. Either way, this process involves a secret signing key.
=> the signature is created by taking the encoded header, encoded payload, and a secret key, and then hashing them together using a specified algorithm => so for "hashing" we need a "secret key".
# symmetric and asymmetric
=> The signing algorithm generates a hash of the JWT's header and payload, and the secret key is used to encrypt the hash, producing the signature.
(symmetric algorithms such as HS256, which use a single shared secret key)
 JWTs are signed using symmetric algorithms such as HS256, which use a single shared secret key. IJWTs are signed using asymmetric algorithms such as RS256, which use a public/private key pair.
=> an access token is used to access a protected resource, while a refresh token is used to obtain a new access token.
=> the payload contain the role of the user: admin, normal user ....
--  
Session: 
* session based authenticated 
* grants access to ALL information available 
* Set-Cookie http headers 
we wall have in the response (session and session.sig) The cookie will still be visible in browser, but it has a signature, so it can detect if the client modified the cookie.
It works by creating a HMAC of the value (current cookie), and base64 encoded it. When the cookie gets read, it recalculates the signature and makes sure that it matches the signature attached to it.
If it does not match, then it will give an error.
the information inside a cookie (like session id) will be "encrypted" using a key. 
=> Using Session (statefull) because we need to store the sessionId in the server (in memory or in the server), it's a downside , so jwt solve this probléme it's only need to validate the signature (stateless)
* Set-Cookie headers vs Cookie headers: 
Set-Cookie header is used by the server to set or update cookies on the client, while the Cookie header is used by the client to send those cookies back to the server with each subsequent request 

Session: Vulnerables to CSRF attack .  
Token: Vulnerables to XSS attack .
-- 

OAuth:
https://www.youtube.com/watch?v=13UbXxAtrbM
OAuth (Open Authorization) - often written as the latest version OAuth 2.0 - is a protocol that is used to authenticate a user via an authentication server. 
When you implement “Sign in with Google” or “Sign in with Github”, you are using the OAuth 2.0 protocol!
How it work (see tof): One of the useful things about OAuth is that it enables you to delegate account access in a secure way without sharing credentials. Instead of credentials, OAuth relies on access tokens.
+ Pros of using OAuth: 
There’s many plug-and-play OAuth options: Including services like “Sign in with Google” and “Sign in with Facebook” that are already set up to be consumed within your application.
+ Cons of using OAuth: 
OAuth can be complicated to understand if you are unfamiliar 
No session management solution. Once the user is authenticated, the auth server simply returns a JWT which can be consumed by your application. However, after that step, the OAuth protocol doesn’t provide any support for specifying how to maintain the authenticated session between your app’s frontend and backend - this is totally up to the developer.

=== Terminology Used by oauth2: 
1- Authorization Code: is when we choose the specific "google account", and we grant access to some scopes provided (allow buttom in google).
The Authorization Code is exchanging for Access Token. 
2- Scope: the data to be accessed by app (for exmaple only email google and photo)
3- Redirect URL: is assign by  the server => the server redirect client to this URL as exmaple /auth
4- Access Token:
the server send it . 
using it we can request userdata as example photo de profil ... 


=> Oauth2 and JWT can be used together.

--- 
# private Ip Address: 
192.168.0.0 – 192.168.255.255 (65,536 IP addresses)
172.16.0.0 – 172.31.255.255 (1,048,576 IP addresses)
10.0.0.0 – 10.255.255.255 (16,777,216 IP addresses)

# URL Encoding
URL encoding is a mechanism for translating unprintable or special characters to a universally accepted format by web servers and browsers.
URL decoding is the reverse process of URL encoding, which transforms a URL that contains special characters into one that doesn't. This makes the address easier to read.
Original URL: https://example.com/my page/index.html
URL Encoded: https://example.com/my%20page/index.html
=> the space in the original URL is replaced by %20 in the URL-encoded version. This ensures that the URL remains valid and can be correctly interpreted by web browsers and servers.
check: https://www.urlencoder.org/

####  1-Information Disclosure (under the Cryptographic Failures categories 2) 
*** Lab: Source code disclosure via backup files. 
* this vulnerability allow you to access information that shoold not be atteinable or visible to you. 
* this BUGS expose sensative information that they can point you to more dangeroues Bugs. 
- the most website (front-end) they have a file called robots.txt (The robots.txt file controls how search engine spiders see and interact with your web pages.) => 
example:
https://pagedart.com/blog/robots-txt-file-example/ 
# feroxbuster (sudo snap install feroxbuster) 
feroxbuster is a tool designed to perform Forced Browsing. Forced browsing is an attack where the aim is to enumerate and access resources that are not referenced by the web application, but are still accessible by an attacker. 
wordlist: (https://raw.githubusercontent.com/danielmiessler/SecLists/master/Discovery/Web-Content/common.txt)  
Note: The wordlist needs to be copied to where feroxbuster is installed (The wordlist needs to be copied to where feroxbuster is installed)
$feroxbuster -u https://0a1e00a9038639c1c1b9c14b00bf0092.web-security-academy.net/ -w /home/amir/snap/feroxbuster/common/SecLists.txt  

*** Lab: Information disclosure in version control history 
also here we use feroxbuster to load all hidden path (using seclist), > .git is availbale , so: 
$ wget -r https://0a46006303bc14ccc0cf8b8a00150046.web-security-academy.net/.git   (wget to download single file, -r rucersivly to all files) 
$git log > to see all commits and commit hash (search fo anythng related to password) 
$git show <commit-hash> > to show all changes in this commit  

*** Lab: Information disclosure in error messages
if we load a parmaters that does nor exist: https://exmaple.com/products?productId=355555 
to See all Hiden requests  we will use "Burp Suite" to intercept request before going to the web application.
Burp Suite Community Edition: Burp is an intercepting HTTP Proxy, with a lot of other features to help you do a security test of a web site.  
The site verbose error messages reveal that it is using a vulnerable version of a third-party framework, so to check that we intercept the request like: 
GET /product?productId=3 HTTP/2 , we manupulate the query parameters with "string" productId=sdcsdcsdcsd => we will receive an error message that contain the error and the "Apache Version". 

*** Lab: Information disclosure on debug page 
The site contains a debug page that discloses sensitive information about the application (exactly the SECRET_KEY)  
> In any site there is a lot of subdirectory and subfiles that we can access "perform Forced Browsing" (feroxbuster as example)
in BURP SUITE > target > select site > Engagement Tools > Discover Cotent .  (there is a built in file list like SecLists in feroxbuster)
so when we did Discover Content we will see an endpoint https://example.com/cgi-bin/phpinfo.php, open this url in the browser and we will get many info inclusing tje SECRET_KEY.

####  2-Broken Access Control Vulnerabilities (the most common security threat in web based on static of OWASP)
Broken access control vulnerability is a type of security flaw that allows an unauthorized user access to restricted resources. 
In simple word : Broken access Control allow you to "Access or modify information beyond limits". 
Broken Access Control is very Large category conatin : 
  * Path Traversal 
  * CSRF 
  * IDOR  
  * Oauth2.0  
  * SSRF
***************IDOR (it's type of broken access control or Access control vulnerabilities)********************************************************

> IDOR stands for "Insecure Direct Object Reference." It is a type of security vulnerability that occurs in web applications when an application provides direct access to objects or data based on user-supplied input, such as a file name or a database key.


*** Lab: User role controlled by request parameter 
in this lab we are going to use a normal user to sign-in, the objectif is to delete another user (but we are not admin). 
This lab has an admin panel at /admin (we can use feroxbuster to determine that 401), which identifies administrators using a forgeable cookie. 
before accessing http://example.com/admin we are going to intercept the request and under "Request Cookies" we will have 2 cookies having sent , one of then is Admin with value false, if we change it to true we will have access to the admin page => Cookie Manipulation. 

*** Lab: User ID controlled by request parameter, with unpredictable user IDs.
In this lab, we are going to sign in a simple user (in the URL bar we will have an id), then we are going to search for a user called carlos , if we Enter to the account of user Carlos we will get his id in the URL bar => then return to home page and change in the URL the id >>> we will get his apy key. 
Trick: if we can not get the other user id, try to create 2 user and change the id for testing. 

*** Lab: User ID controlled by request parameter with password disclosure 
log in and you will have something in the URL 
https://0aac0070048c4b82c00f950800c90054.web-security-academy.net/my-account?id=wiener 
change wiener to administrator, then the password will appears; take it from the inspect element; then logout and log in as administrator .  

*** Lab: IDOR (Insecure Direct Object Reference) 
=> Objects(Doc, images, Databases Records) are accessed directly, very simple (when we copy a private pdf file as example and load this url in private chrome and it loaded)
  
*** Lab im: User role can be modified in user profile 
in this Lab we will send a post request (using burp suite exactly repeater) to change email, and the response will return the content in the body parameters new email, username and roleid=1 => so we can send a new roleid=2 whih is the role id of the admin . 

*** Lab im: Authentication bypass via information disclosure 
at /admin: we will have this message: Admin interface only available to local users
TRACE: when we send a trace to the target webserver , the target server will response by a simple response conatin the request tha we sent to it. 
this is very useful, "because it let us understand whether the request send to the server got modified or not". 
in this  lab there is a header "X-Custom-IP-Authorization": that contain our ip address 
=> to solve this probléme we will add to the request X-Custom-IP-Authorization: 127.0.0.1 

***************PAth Traversal (it's type of broken access control or Access control vulnerabilities)*******************************************************
Allow you to access or modify path (or files) beyond you limits. 
*** Lab: File path traversal, traversal sequences blocked with absolute path bypass
To solve the lab, we need to retrieve the contents of the /etc/passwd file.
if we load this path: https://0a7f007f03552b46c138bd9500aa0040.web-security-academy.net/product?productId=2 
we can see that it load an image at GET /image?filename=3.jpg , so if we change to GET /image?filename=/etc/passwd 
we will have all password on the server .
=> we load file outside the webroot . 

*** Lab : File path traversal, simple case
This lab contains a file path traversal vulnerability in the display of product images.
some time, this will not work but the target will still vulnerabile. for example the web server is allow me to load file from a certain directory (/var/www/html/image), so we are going to test not the absolute path , but ../etc/passwd or ../../etc/passwd ... until we got the file  

*** Lab im: File path traversal, validation of file extension with null byte bypass
This lab contains a file path traversal vulnerability in the display of product images.
some times, we are not  able to load such file like /etc/passwd for many reason example "filtering" (restriction to load only image ...) => the target is expect only to load images. 
so we can use an null byte: "%00" 
../../../etc/passwd%00.jpg => (ended with .jpg) here the target websites will filter and it will check if the uri end with "jpg" and it will pass it. 
but then when it acutually exectued , the processing will end when it see %00 (it will become ../../../etc/passwd ) 
solution will be loaded: filename=../../../etc/passwd%00.jpg 

*** Lab: File path traversal, traversal sequences stripped non-recursively (bypassing filtering) 
Some all what we use will not working , so maybe the admin of target host will analyse the request send and filterning "../" means it removed from the request so we can double everthing we send it  ....//....//....//etc/passwd. one will be removed and the other will persist.

*** Lab :  File path traversal, validation of start of path (Bypassing hard-coded Paths) 
This lab contains a file path traversal vulnerability in the display of product images.
In this Lab insteade of sending GET /image?filename=/var/www/images/9.jpg => GET /image?filename=/var/www/images/../../../etc/passwd 

*** Lab im: File path traversal, traversal sequences stripped with superfluous URL-decode (Bypassing advanced fIltering) 
This lab contains a file path traversal vulnerability in the display of product images.
we are assuming that the web application have a kind of firewall or filtering that's checking if the value sended to it contain "/", so we are going to pass the forward slach differently "URL encoded", but still get exedcuted as forward slash. 
GET /image?filename=..%2f..%2f..%2fetc%2fpasswd
---
Maybe also the firewall can be smart and checking if the url in encoded => so we are to double encod it. 
GET /image?filename=..%25%32%66..%25%32%66..%25%32%66etc%25%32%66passwd 
check: directory-traversal-cheatsheet.txt 

*** Lab: Automate task of Path Traversal 
manually "directory-traversal-cheatsheet.txt" using this sheets can be boring , we can use the "Intruder" in BurpSuite. (BurpSuite pro is more efficient, we don't need to use sheets, just "Fuzzing-path traversal") 

***************CSRF (Cross-Site Request Forgery (CSRF) ) (it's type of broken access control)********************************************************
https://www.youtube.com/watch?v=7bTNMSqCMI0&list=PLuyTk2_mYISKNFqao_NBzYOWvJFqhVmXN

also called Client-side Request Forgery, it's happen when Server does not properly check if the user generated the request. 
CSRF is an attack where the attacker causes the victim user to carry out an action unintentionally while that user is authenticated (means user need to be already authenticated).

How to Prevent CSRF ? See tof.
=> Request can be forged and sent to users to make them take action they don't intend to do such as : 
          - change password 
          - change email 
          - submit a payment 

*** lab: CSRF vulnerability with no defenses
Vulnerable parameter - email change functionality
Goal - exploit the CSRF vulnerability and change the email address
Analysis:

In order for a CSRF attack to be possible:
- A relevant action - email change functionality
- Cookie based session handling - session cookie
- No unpredictable request parameters (like csrf token) - satisfied 
to generate the CSRF script there is a feature in Burp , click right on the request => Engagement tools => Generate CSRF PoC , then option and "Auto submit script"  
this script need to be hosted on your own server and whenever the user click the link the script will be exectued. 
you can use python3 to host this script: "$python3 -m http.server 8000" , the html file need to be in the same path where the html is present. 
# Note: "Auto submit script" means "submit a hidden form" will work even you have stric "CORS" 

=> Create a PoC script to exploit CSRF: 
 * GET request: <img> tag with src attribute set to vulnerable URL. (we shoold net use Get request to "submit data to the applications")
 * POST request: from with hidden fields for all the required parameters and the target set to vulnerable URL. ( the "form element to submit the request")

*** lab: CSRF where token validation depends on request method 
Common defences against CSRF: (3 methods)

    see video CSRF Tokens:   
    * CSRF tokens - A CSRF token is a unique, secret, and unpredictable value that is generated by the server-side application and shared with the client. When attempting to perform a sensitive action, such as submitting a form, the client must include the correct CSRF token in the request. This makes it very difficult for an attacker to construct a valid request on behalf of the victim
    => CSRF token is a type of validation that is not send with request (to server) and not saved in the cookie storage or local storage, so hwo it work? we genrate a random token in the backend(server) and send it to the client (browser) in the request, we store this token as variable(in javascript code), so whenever we want to send a request to the server, we send this token in the body of the request and we check it validation in the server(session.csrfToken == req.body.csrfToken). we check not only the session mutch but also the token match. 
    in backend will be: SESSIONS.set(sessionId, {user, csrfToken}) 
    - in resume: 
    * CSRF token shoold be tied to the user's session. 
    * shoold be validated before the relevant action is executed 

    see video samesite attributes: https://www.youtube.com/watch?v=VtJcd5JXjio
    * SameSite cookies - SameSite is a browser security mechanism that determines when a website's cookies are included in requests originating from other websites, samesite need to be send from the server(backend) with the cookie (you have 3 choices: strict, lax, none) 
    * in simple world: SameSite Controls in which cases cookies are sent in cross site requests (from domain1.com to domain2.com)
    => it's new features (not all browsers support it yet), if you don't add SameSite in the server, the browsers will added it as LAX 
    => have a "Lax" policy which would have prevent csrf attack even without a csrf token. (a little complicated ) 
    => example: const cookie = "user=amir(sessionId); samesite=strict; secure" 
    => if you don't have the samesite attributes, chrome will treated it as LAX. 
    => with Lax, cookie will be sent only with "safe http verbs" means GET, HEAD, TRACE .
       
    * Referer-based validation - Some applications make use of the HTTP Referer header to attempt to defend against CSRF attacks, normally by verifying that the request originated from the application's own domain. (see general on top).

This lab's email change functionality is vulnerable to CSRF. It attempts to block CSRF attacks, but only applies defenses to certain types of requests (GET/POST)
Analysis:

In order for a CSRF attack to be possible:
- A relevant action: change a users email
- Cookie-based session handling: session cookie
- No unpredictable request parameters: Request method can be changed to GET which does not require CSRF token

Testing CSRF Tokens:
Change the request method from POST to GET (To check whether I can change the email "with a GET request", I send the password change request to Repeater and select Change request method from the context menu. Sure enough, the email changes)


*** Lab: CSRF where token validation depends on token being present 
if we remove the CSRF token from post request, the request will succefful. 