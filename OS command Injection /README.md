# Injection Vulnerability 
There is various type of injection vulnerabilities: 
* OS command injection 
* XSS (Cross-site scripting)
* SQL Injection  
# OS Command Injection 
In Simple word: Execute system commands on the target web server. (Include application and server, also network and other resources). 
Command Injection Cheatsheat: https://hackersonlineclub.com/command-injection-cheatsheet/
Blind: means that the application does not return the output from the command within its HTTP response. 

*** Lab: OS command injection, simple case #python scripting
This lab contains an OS command injection vulnerability in the product stock checker.

The application executes a shell command containing user-supplied product and store IDs, and returns the raw output from the command in its response. 

*** Lab: Blind OS command injection with time delays
To solve the lab, exploit the blind OS command injection vulnerability to cause a 10 second delay.
Command Injection Cheatsheat: https://hackersonlineclub.com/command-injection-cheatsheet/     => we can use the intruder for that 

*** Lab im: Blind OS command injection with output redirection  #python script
Target Goal - Exploit the blind command injection and redirect the output from the whoami command to the /var/www/images

Analysis:

1. Confirm blind command injection
- normally with command like "sleep 10" 

2. Check where images are store (it's stored under /var/www/images/)

3. Redirect output to file

4. Check if file was created 

*** Lab advanced: Blind OS command injection with out-of-band interaction 
IN some case, we can have "Blind OS command Injection" where the command is going to be executed on target server, but in "separate thread", so as an exapmle "sleep 10", that command is going to be executed in separate threat, so the app is going to work normally (no delay of 10s)
so if we inject "nslookup my-own-server", then go to my-own-server and see the log , we can observe that the "target website" execute an nslookup. (#to do)