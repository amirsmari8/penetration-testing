# Injection Vulnerability 
There is various type of injection vulnerabilities: 
* OS command injection 
* XSS (Cross-site scripting)
* SQL Injection   
# XSS (Cross-site scripting) 
Allow an attacker to inject javascript code into the page. 
Code is executed when tha page loads. 
Code is executed on the client machine not the server. 
there are 3 main types: 
    1- Reflected XSS 
    2- Persistent/Stored XSS 
    3- DOM based XSS 
# 1- Reflected XSS: 
=> None persistent, not stored. 
=> Only work if the target visits a specially crafted URL. 
=> ex: http://target.com/page.php?something=<script>alert("XSS")</script> 
# 2- Persistent/Stored XSS: 
=> Persistent, stored on the page or DB 
=> The inject code is executed evertime the page is loaded as an example if we discover a stored XSS in google, the code will be executed on every "browser" visited google. 
# 3- DOM based XSS: 
=> Similar to reflected and stored XSS. 
=> Main difference is that it occurs entirely on the client side. 
=> Payload is never sent to the server 