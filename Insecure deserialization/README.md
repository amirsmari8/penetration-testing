 # Serialization and deserialization 
 Serialization and deserialization are two processes that are often used in computer programming to store and retrieve objects and data in a way that can be easily transmitted or persisted.
# Serialization 
Serialization refers to the process of converting an object or data structure in memory to a byte stream (0110001100), so that it can be easily stored or transmitted across a network. 
# deserialization
Deserialization, on the other hand, is the process of reconstructing an object or data structure from a byte stream that was previously serialized. This involves reading the byte stream and reconstructing the object or data structure in memory. 

______________________________________________________ 
# Insecure deserialization 
Insecure deserialization is a vulnerability that can occur when an application "deserializes" data without properly validating and filtering it. This can allow attackers to modify serialized data in a way that causes the application to execute malicious code or access sensitive information.
# How to prevent Insecure deserialization 
To prevent insecure deserialization attacks, it is important to validate and filter all input data, limit the use of deserialization, and use secure serialization formats and libraries. 


# PHP serialization format
PHP uses a mostly human-readable string format, with letters representing the data type and numbers representing the length of each entry. For example, consider a User object with the attributes:  
$user->name = "carlos";
$user->isLoggedIn = true; 
 When serialized, this object may look something like this: 
 O:4:"User":2:{s:4:"name":s:6:"carlos"; s:10:"isLoggedIn":b:1;} 
  This can be interpreted as follows:

    O:4:"User" - An object with the 4-character class name "User"
    2 - the object has 2 attributes
    s:4:"name" - The key of the first attribute is the 4-character string "name"
    s:6:"carlos" - The value of the first attribute is the 6-character string "carlos"
    s:10:"isLoggedIn" - The key of the second attribute is the 10-character string "isLoggedIn"
    b:1 - The value of the second attribute is the boolean value true

The native methods for PHP serialization are serialize() and unserialize(). If you have source code access, you should start by looking for unserialize() anywhere in the code and investigating further. 

*** Lab:  
This lab uses a serialization-based session mechanism and is vulnerable to privilege escalation as a result. 
in the "GET /my-account" request contains a "session cookie" that appears to be URL and Base64-encoded. 
if we decode it: 
echo -n "Tzo0OiJVc2VyIjoyOntzOjg6InVzZXJuYW1lIjtzOjY6IndpZW5lciI7czo1OiJhZG1pbiI7YjowO30%3d" |base64 -d
we will have: 
O:4:"User":2:{s:8:"username";s:6:"wiener";s:5:"admin";b:0;}base64: invalid input
the value of the second attribute "admin" is false (b:0), if we change to 1 , it will be true 

*** Lab: Modifying serialized data types 
This lab uses a serialization-based session mechanism and is vulnerable to authentication bypass as a result 
in the "GET /my-account" request contains a "session cookie" , if we  decode it base64 

O:4:"User":2:{s:8:"username";s:6:"wiener";s:12:"access_token";s:32:"uj164h1vz1isph9lray2pls4l3wkwv7t";fQ%3d%3d

explanation: 
The string  appears to be a serialized object in PHP programming language. It represents an object of the class "User" with two properties:

"username": a string with the value "wiener" (6 characters).
"access_token": a string with the value "uj164h1vz1isph9lray2pls4l3wkwv7t" (32 characters).

=> to hack this we need: 

    Update the length of the username attribute to 13.
    Change the username to administrator.
    Change the access token to the integer 0. As this is no longer a string, you also need to remove the double-quotes surrounding the value.
    Update the data type label for the access token by replacing s with i.
     
The result should look like this:
O:4:"User":2:{s:8:"username";s:13:"administrator";s:12:"access_token";i:0;} 
