### Practise Lab 
https://portswigger.net/web-security/all-labs

# JSON Web Token Attacks 
=> The signing algorithm(symmetric algorithms such as HS256, which use a single shared secret key) generates a hash of the JWT's header and payload, and the secret key is used to encrypt the hash, producing the signature.
=> The server that issues the token typically generates the signature by hashing the header and payload. In some cases, they also encrypt the resulting hash. Either way, this process involves a secret signing key
=>  JWTs are signed using symmetric algorithms such as HS256, which use a single shared secret key. IJWTs are signed using asymmetric algorithms such as RS256, which use a public/private key pair.
 JWTs are signed using symmetric algorithms such as HS256, which use a single shared secret key. IJWTs are signed using asymmetric algorithms such as RS256, which use a public/private key pair.


*** lab: JWT authentication bypass via unverified signature 
Note: session cookie is a JWT
We need to install the JWT editor extention from burp suite
This lab uses a JWT-based mechanism for handling "sessions". Due to implementation flaws, the server doesn't verify the signature of any JWTs that it receives. 
we recommend installing the JWT Editor extension, which is available from the BApp Store 
in the payload field, change "wiener" to "administrator" then send the jwt (if we do this the signature sended dosent change as example xxx), normally the server will get the pyload and headers with generate the signtaure and compare it with the signature sended in the request, but this didn't happen.

*** lab: JWT authentication bypass via flawed signature verification
https://www.youtube.com/watch?v=rEUoU6OYH_g
Note: session cookie is a JWT 
in the header of the JWT , we change the algorithme to "none" and we remove the signature from the JWT. 
The "JWT None" algorithm attack is a type of vulnerability that arises when a JWT (JSON Web Token) is signed using the "None" algorithm. 
=> The server is insecurely configured to accept unsigned JWTs.
=> No signature is verified. 

*** lab im : JWT authentication bypass via weak signing key
Note: session cookie is a JWT
the website uses an extremely weak secret key to both sign and verify tokens. This can be easily brute-forced using a wordlist of common secrets. 
JWT common secret list: https://github.com/wallarm/jwt-secrets/blob/master/jwt.secrets.list 
=> "Hashcat" is a powerful tool that helps to crack password hashes. Hashcat supports most hashing algorithms and can work with a variety of attack modes 
in this lab we're going to get  "key" used to hash the "jwt signature" using "hashcat" and secret list , then we're going to Modify (in the payload add administrator) and sign the JWT. 

1- Brute-force the secret key:
$ hashcat -a 0 -m 16500 <YOUR-JWT> /path/to/jwt.secrets.list
-a: attack mode=0=Straight 
-m: --hash-type=16500=JWT 
this should reveal that the weak secret is secret1. 

2- Generate a forged signing key:
first we need to Base64 encode the secret. 
then, we need to Generate new Symmetric Key (this Symmetric Key is needed to sign the Token )

*** Lab: JWT authentication bypass via jwk header injection 
https://andyatkinson.com/blog/2021/05/12/jwt-jwk-jwks
to do