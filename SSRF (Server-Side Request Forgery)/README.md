# SSRF: Server Side Request Forgery 
# what's an SSRF: 
a lot of web server do much more than serving image, executing code ... , they can communicate with other server . 
CSRF where we manipulate the client request and get "the client" to submit the request , but SSRF we manipulate the "web server"
# Type of SSRF vulnerability 
two 2 type of SSRF: 
    * Regular/ In Band: if we request an URL, and the content of the URL is "diplayed back" as response in the application.
    * Blind/ Out-of-Band: the application "dosen't" display back the response.  
# Impact of SSRF Attacks 
depend on the functionality in the application . 
can lead to sensitive information disclosure, scan of internal network, comporomise of internal services, remote code execution ... 
# How to Exploit SSRF vulnerability 
=> depend on type (Regular, Blind): 
# Prevent SSRF Vulnerabilities 
Defense in depth approach: 
    * Application Layer defenses 
    * Network layer Defense 
--- 
*** Lab im: Basic SSRF against the local server     
this "Web server" that's run the website is sending another request to "http://stock.weliketoshop.net/product?productId=5" , so if we change the URL (http://stock.weliketoshop.net/product?productId=5) to localhost (means localhost of the web server) => admin application is running on the same server .
Note: maybe you need to URL encode (http://localhost/) 
TODO , you must did the lab  

*** Lab im: Basic SSRF against another back-end system   # scan and mapping internal network
To solve the lab, use the stock check functionality to scan the internal 192.168.0.X range for an admin interface on port 8080, then use it to delete the user carlos => the "web server" is sending a request to another server an internal private ip address "192.168.1.1" on port "8080" , so we're going to use the ""intruder" to scan for "192.168.1.1 > 192.168.1.254" (also the port)
=> the admin panel in runing on 192.168.1.167:8080/admin

